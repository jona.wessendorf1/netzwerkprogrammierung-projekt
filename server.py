from requests import get
from threading import Thread
from time import sleep, time
from flask import Flask
from argparse import ArgumentParser
from multiprocessing import Process
from config import host_list, delay, only_run_minor_on_quorum, run_on_master, run_on_minor


alive_servers = []
master = None
was_master_after_last_quorum = None
process = None
HOST = ''
PORT = ''

app = Flask(__name__)

@app.route('/alive')
def alive():
    return ""


def check_if_server_is_alive(host, port):
    """
    This method checks if a server is alive and reachable.
    :param host: The hostname of the server
    :param port: The port of the server
    :return: Returns if the server is alive and reachable
    """
    try:
        r = get(f'http://{host}:{port}/alive', timeout=min(max(delay/2, 500), 3000))
        if r.status_code == 200:
            # server is reachable
            return True
    except:
        pass
    # server is not reachable
    return False


def get_master(all_servers, alive_servers):
    """
    This method checks if enough servers are online to build a quorum
    and determins the master if this is the case.
    :param all_servers: A list of the hostnames and ports of all servers
    :param alive_servers: A list of the hostnames and ports of all the servers that are alive
    :return: Returns the address of the master. If no quorum could be generated None is returned
    """
    # sorting the alive servers for a determenistic selection of the master
    alive_servers = sorted(alive_servers)

    # checking if enough servers are online to build a quorum
    if len(all_servers) / 2 >= len(alive_servers):
        return None

    # since the list of alive servers is sorted the first element is determenistic
    return f'{alive_servers[0][0]}:{alive_servers[0][1]}'


def start_master_or_minor_if_not_started():
    """
    This method starts a process for the master or minor service if it is not yet started.
    """
    global master, was_master_after_last_quorum, process

    # handeling the case of a quorum disappearing
    if master == None and only_run_minor_on_quorum:
        was_master_after_last_quorum = None
        if process != None:
            process.terminate()
        return

    now_master = master == f'{HOST}:{PORT}'

    if now_master != was_master_after_last_quorum:
        # terminating the old process if it exists (a SIGTERM signal will be sent)
        if was_master_after_last_quorum != None and process != None:
            process.terminate()

        # starting process for the master or minor script
        was_master_after_last_quorum = now_master
        process = Process(target=run_on_master if now_master else run_on_minor)
        process.start()
        print(f"{'Master' if now_master else 'Minor'} script started")


def update_alive_servers():
    """
    This method updates the list of alive servers and after uptating it, the master will be determined.
    """
    global alive_servers, master

    # checking which servers are reachable
    online_servers = []
    for host, port in host_list:
        if check_if_server_is_alive(host, port):
            online_servers.append((host, port))

    alive_servers = online_servers
    master = get_master(host_list, alive_servers)

    print(f"{len(alive_servers)} out of {len(host_list)} servers alive. Master is {'Me' if master == f'{HOST}:{PORT}' else master}")

    start_master_or_minor_if_not_started()


def update_worker():
    """
    This method calls the update_alive_servers method in a provided interval.
    It should be used in its own thread.
    """
    while True:
        # this is used to sync the time the requests are sent
        current_time_in_ms = round(time() * 1000)
        wait_time_in_ms = delay - (current_time_in_ms % delay)
        sleep(wait_time_in_ms / 1000)

        update_alive_servers()


def start_server(host:str, port:int):
    """
    This method is used to start the server and the update thread.
    :param host: The hostname of the server.
    :param port: The port of the server.
    """
    global HOST, PORT
    HOST = host
    PORT = port

    # check if the HOST and PORT are found in the configuration list of servers
    if (HOST, PORT) not in host_list:
        print(f'{HOST}:{PORT} is not configured as a host. You need to add the ip and the port to the config.py file.')
        return

    # start the update thread
    update_thread = Thread(target=update_worker)
    update_thread.start()

    # run the flask app
    app.run(host=HOST, port=PORT)

    update_thread.join()


if __name__ == '__main__':
    parser = ArgumentParser(description='A service server that can build a quorum together with other services')
    parser.add_argument('host', type=str, help='hostname of the server')
    parser.add_argument('port', type=int, help='port of the server')
    args = parser.parse_args()

    start_server(args.host, args.port)
