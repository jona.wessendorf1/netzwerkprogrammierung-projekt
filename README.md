# Netzwerkprogrammierung Projekt

## Content
1. [Description](#description)
2. [Installation](#installation)
3. [Usage](#usage)
4. [Configuration](#configuration)
5. [Author](#author)
6. [License](#license)

# Description
This project implements a protocol for distributed systems to select a master among multiple equivalent servers (similar to Openstack, Galera, Cepth, etc.). 
If enough servers are online they will find a master which can execute some specific code. Once the master is stopped the remaining servers will determine a new master as long as enough servers are online.
This mechanism provides an excelent uptime for the whole system.

# Installation
In order to set up the project, follow the following steps in order.

## Cloning the Project

```bash
git clone https://gitlab.ub.uni-bielefeld.de/jona.wessendorf1/netzwerkprogrammierung-projekt.git
cd netzwerkprogrammierung-projekt
```

## Setting up a virtual environment for python

```bash
python3 -m venv venv
source venv/bin/activate
```

## Installing the required packages

```bash
pip3 install -r requirements.txt
```

## Running the tests

```bash
python3 -m unittest tests
```

# Usage
Before executing any commands in the shell, always make sure the virtual environment is sourced.
```bash
source venv/bin/activate
```

## Starting a single instance
```bash
python3 server.py <hostname> <port>
```

## Example (working with the provided configuration)
This small example will show the capabilities of the project.

First open up four terminals on a machine.

### Starting the first instance
```bash
python3 server.py 127.0.0.1 13371
```
After waiting for a maximum time of 2 seconds, a message will appear that the minor script was started.

### Starting the second instance
```bash
python3 server.py 127.0.0.1 13372
```
After waiting for a maximum time of 2 seconds, a message will appear that the minor script was started.

### Starting the third instance
```bash
python3 server.py 127.0.0.1 13373
```
After starting the third instance, a quorum can be build. You will see that the first instance displayed a message that it is running the master script now.

### Starting the fourth instance
```bash
python3 server.py 127.0.0.1 13374
```

### Stopping the first instance
This can be achieved by pressing Crtl-C two times in the terminal.
After the first instance was stopped, the second instance will be selected as the master.

# Configuration
To configure the project, edit the `config.py` file.
- `host_list` The host list contains all the hosts specified by the hostname and port.
- `delay` The delay is the time (in ms) between the alive pings to the other servers.
- `only_run_minor_on_quorum` If set to true, the services will only run the minor script if a quorum was build.
- `run_on_master` This function will be executed if a service becomes the master. It can be configured to execute some python code, or you could also execute some shell script from this function.
- `run_on_minor` This function will be executed if a service becomes a minor. It can be configured in the same way as the `run_on_master`.

# API
The server has a single API path: `/alive`. This is used to check if a server is online and reachable. The successful response is an empty string.

# Author
Jona Wessendorf

# License
This project is licensed under the MIT license.
