from multiprocessing import Process
import os
import sys
from time import sleep
import unittest
from server import get_master, check_if_server_is_alive, app

class TestMethods(unittest.TestCase):
    def test_empty_lists_no_master(self):
        self.assertEqual(get_master([], []), None)
        pass

    def test_master_one_element_in_list(self):
        self.assertEqual(get_master(
            [("host", 1234)],
            [("host", 1234)]),
            "host:1234")
        pass

    def test_master_to_few_elements_in_alive_list(self):
        self.assertEqual(get_master(
            [("host", 1234), ("host", 1235)],
            [("host", 1234)]),
            None)
        pass

    def test_master_two_elements_in_both_lists(self):
        self.assertEqual(get_master(
            [("host", 1234), ("host", 1235)],
            [("host", 1234), ("host", 1235)]),
            "host:1234")
        pass

    def test_master_two_elements_in_both_lists_unsorted(self):
        self.assertEqual(get_master(
            [("host", 1234), ("host", 1235)],
            [("host", 1235), ("host", 1234)]),
            "host:1234")
        pass

    def test_master_four_elements_in_all_list_and_only_two_online_servers(self):
        self.assertEqual(get_master(
            [("host", 1234), ("host", 1235), ("host", 1236), ("host", 1237)],
            [("host", 1235), ("host", 1234)]),
            None)
        pass

    def test_is_alive_method_working(self):
        def run_app():
            # disabled console output for flask app
            f = open(os.devnull, 'w')
            sys.stdout = f
            sys.stderr = f

            app.run(host="127.0.0.1", port=12345)

        p = Process(target=run_app)
        p.start()

        # waiting for the server to start
        sleep(3)

        self.assertTrue(check_if_server_is_alive("127.0.0.1", 12345))
        p.terminate()

    def test_is_alive_method_failing(self):
        self.assertFalse(check_if_server_is_alive("127.0.0.1", 54321))

if __name__ == '__main__':
    unittest.main()
