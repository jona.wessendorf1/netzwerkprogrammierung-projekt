# The list containing all server hostnames and ports
from time import sleep


host_list = [
    ("127.0.0.1", 13371),
    ("127.0.0.1", 13372),
    ("127.0.0.1", 13373),
    ("127.0.0.1", 13374),
    ("127.0.0.1", 13375)
]

# The delay between alive calls in ms
delay = 2000

# This can be used to configure if the minor script will only run if a quorum was found
only_run_minor_on_quorum = False

# This can be configured to run anything on the master service
# The function will be called from its own process so it can be blocking
# If the service is no longer master a SIGTERM signal will be sent
def run_on_master():
    while True:
        print("This could be output from the MASTER service")
        sleep(1)

# This can be configured to run anything on the minor services
# The function will be called from its own process so it can be blocking
# If the service is no longer minor a SIGTERM signal will be sent
def run_on_minor():
    while True:
        print("This could be output from the MINOR service")
        sleep(1)
